# Gotrend Assets
Our slogan is "Make your choices easy" for a reason; here at Gotrend, we are all about customer experience. Combining premier fashion choices with excellent usability, we make it as easy as possible for both customers and partners to find what they need – each and every item.

## Repository Content
 - Gotrend Colors (SCSS, CSS)
 - Gotrend Iconic Font
 - Gotrend Logo (AI, SVG, PNG)

## Installation
```sh
$ npm install git+ssh://git@gitlab.com:sabriayes/gotrend-assets.git#v1.0.2 --save
# Or
$ npm install git+ssh://git@gitlab.com:sabriayes/gotrend-assets.git --save
```
## Gotrend Colors

Color plays an important role in the Gotrend corporate identity. The colors below are recommendations for various media. A palette of primary colors has been developed, which comprise the "One Voice" color scheme.

<img src="https://corporate.gotrend.com/img/color-orange@2x.jpg" width="140"/>
<img src="https://corporate.gotrend.com/img/color-black@2x.jpg" width="140"/>

Import SCSS or CSS file your project. It contains Gotrend brandimg colors code.
```css
/** Import in CSS file */
@import ./gotrend-assets/dist/colors.scss
```
```html
<!-- or in HTML header -->
<link rel="stylesheet" href="./gotrend-assets/dist/colors.css">
```
```js
/** or Angular 8+ (update angular.json) */
"styles": [
    "node_modules//gotrend-assets/dist/colors.css"
]
```
Usage Gotrend colors.
```html
<!-- Text colors -->
<span class="gotrend-primary-color">Gotrend Primary Text Color</span>
<span class="gotrend-secondary-color">Gotrend Secondary Text Color</span>

<!-- Background colors -->
<div class="gotrend-primary-bg-color"> ... </div>
<div class="gotrend-secondary-bg-color"> ... </div>
```
## Gotrend Iconic Font
Import SCSS or CSS file your project. Font family is "Gotrend Iconic". It contains 144 outline icons.
```css
/** Import in CSS file */
@import ./gotrend-assets/dist/gotrend.css
```
```html
<!-- or in HTML header -->
<link rel="stylesheet" href="./gotrend-assets/dist/gotrend.css">
```
```js
/** or Angular 8+ (update angular.json) */
"styles": [
    "node_modules/gotrend-assets/dist/gotrend.css"
]
```
Usage Gotrend Iconic font.
```html
<!--
    .gotrend -> font family class
    .icon-user -> icon class
-->
<span class="gotrend icon-user"></span>
```
# Gotrend Logo
Our Logo is the key building block of our identity, the primary visual element that identifies us. The signature is a combination of the the symbol itself and our company name – they have a fixed relationship that should never be changed in any way, when mentioning "Gotrend" in your copy.

 - Display the word "Gotrend" in the same font size and style.
 - Capitalize the word "Gotrend" except when it’s part of a web address.

<img src="https://corporate.gotrend.com/img/main-logo@2x.png" width="280"/>
<img src="https://corporate.gotrend.com/img/logo-option@2x.png" width="280"/>


See [http://corporate.gotrend.com/brand-resources](http://corporate.gotrend.com/brand-resources)

| Type | File |
| ------ | ------ |
| AI | gotrend-assets/dist/assets/logos/logo.ai |
| SVG | gotrend-assets/dist/assets/logos/logo.svg |
| PDF | gotrend-assets/dist/assets/logos/logo.pdf |

# Media Releases

Download from official website [https://corporate.gotrend.com/img/media-release.eps](https://corporate.gotrend.com/img/media-release.eps)

<img src="https://corporate.gotrend.com/img/media-pic@2x.png" width="280"/>

| Type | File |
| ------ | ------ |
| EPS | gotrend-assets/dist/assets/media-releases.eps |

# Favicons

| Type | Px | File |
| ------ | ------ | ------ |
| ICO | 48x48 | gotrend-assets/dist/assets/facicon.ico |
| PNG | 32x32 | gotrend-assets/dist/assets/facicon.png |
| PNG | 16x16 | gotrend-assets/dist/assets/facicon-16x16.png |
| PNG | 16x16 | gotrend-assets/dist/assets/apple-touch-icon.png |
| PNG | 512x512 | gotrend-assets/dist/assets/android-chrome-512x512.png |
| PNG | 192x192 | gotrend-assets/dist/assets/android-chrome-192x192.png |

